import _root_.data.DataStore
import _root_.data.models.{PhoneNumber, Channel, User}
import _root_.helpers.Util.getUUID
import play.api.mvc._
import play.api._

object Global extends GlobalSettings {

  val rawPhoneNumbers = List("91-8800646754", "91-8800646755", "1-3308946776", "1-3308946780", "2-6509846709", "12-8989898989", "32-4545454545")
  val channelNames = List("Channel1", "Channel2", "Channel3")
  val userNames = List("User1", "User2", "User3", "User4", "User5", "User6", "User7")

  override def onStart(app: Application) = {
    // fill data store
    println("filling data store")
    rawPhoneNumbers.foreach(PhoneNumber.addNew)
    channelNames.foreach(Channel(getUUID, _, None).insert())
    userNames.foreach(User(getUUID, _).insert())
  }

}
