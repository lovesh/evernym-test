package controllers

import java.util.UUID
import _root_.data.DataStore
import _root_.data.models.{Broadcast, User, Channel, PhoneNumber}
import org.joda.time.{DateTimeZone, DateTime}
import play.api._
import play.api.libs.json.{JsString, Writes, JsValue, Json}
import play.api.mvc._
import play.api.data._
import play.api.data.Forms._
import _root_.helpers.Util.{getUUID, phoneNumberChooser}
import scala.concurrent.Future
import scala.util.{Failure, Success}
import play.api.i18n.Messages.Implicits._
import play.api.Play.current
import scala.collection.mutable.{Set => MSet}


class Application extends Controller {

  def index = Action {
    Ok(views.html.index("Your new application is ready."))
  }

  implicit val uuidWrites = new Writes[UUID] {
    def writes(u: UUID): JsValue = JsString(u.toString)
  }

  implicit val phoneNumberWrites = new Writes[PhoneNumber] {
    def writes(pn: PhoneNumber): JsValue = {
      Json.obj(
        "number"-> pn.number
      )
    }
  }

  case class PhoneNumberFormData(number: String)

  def phoneNumberForm = Form(
    mapping(
      "number" -> nonEmptyText
    )(PhoneNumberFormData.apply)(PhoneNumberFormData.unapply)
  )

  def addPhoneNumber = Action.async { implicit request =>
    phoneNumberForm.bindFromRequest.fold(
      formWithErrors => Future.successful(BadRequest(formWithErrors.errorsAsJson)),
      validForm => {
        PhoneNumber.addNew(validForm.number) match {
          case Success(pn: PhoneNumber) =>
            Future.successful(Ok(Json.toJson(Json.obj(
              "success"-> true,
              "phoneNumber"-> Json.toJson(pn)
            ))))

          case Failure(ex) =>
            Future.successful(BadRequest(Json.toJson(Json.obj(
              "success"-> false,
              "message"-> ex.getMessage
            ))))
        }
      }
    )
  }

  implicit val channelWrites = new Writes[Channel] {
    def writes(cn: Channel): JsValue = {
      Json.obj(
        "id"-> cn.id,
        "name"-> cn.name,
        "phoneNumber"-> cn.phoneNumber
      )
    }
  }

  case class ChannelFormData(name: String)

  def channelForm = Form(
    mapping(
      "name" -> nonEmptyText
    )(ChannelFormData.apply)(ChannelFormData.unapply)
  )

  def addChannel = Action.async { implicit request =>
    channelForm.bindFromRequest.fold(
      formWithErrors => Future.successful(BadRequest(formWithErrors.errorsAsJson)),
      validForm => {
        val channel = Channel(getUUID, validForm.name, None)
        channel.insert()
        Future.successful(Ok(Json.toJson(Json.obj(
          "success"-> true,
          "channel"-> Json.toJson(channel)
        ))))
      }
    )
  }

  implicit val userWrites = new Writes[User] {
    def writes(user: User): JsValue = {
      Json.obj(
        "id"-> user.id,
        "name"-> user.name
      )
    }
  }

  case class UserFormData(name: String)

  def userForm = Form(
    mapping(
      "name" -> nonEmptyText
    )(UserFormData.apply)(UserFormData.unapply)
  )

  def addUser = Action.async { implicit request =>
    channelForm.bindFromRequest.fold(
      formWithErrors => Future.successful(BadRequest(formWithErrors.errorsAsJson)),
      validForm => {
        val user = User(getUUID, validForm.name)
        user.insert()
        Future.successful(Ok(Json.toJson(Json.obj(
          "success"-> true,
          "user"-> Json.toJson(user)
        ))))
      }
    )
  }

  def followChannel(userId: String, channelId: String) = Action.async { implicit request =>
    (getUUID(userId), getUUID(channelId)) match {

      case (Success(uid: UUID), Success(cid: UUID)) =>
        val channel = Channel.findById(cid)
        if (channel.isEmpty)
          Future.successful(Ok(Json.toJson(Json.obj(
            "success"-> false,
            "message"-> "No channel with given id"
          ))))
        else {
          synchronized {
            // if channel does not have phone number
            if (channel.get.phoneNumber.isEmpty) {
              channel.get.userStartsFollowing(uid)
              Future.successful(Ok(Json.toJson(Json.obj(
                "success"-> true,
                "channel"-> channel
              ))))
            }
            // if channel has a phone number
            else {
              val channelsFollowedByUser = DataStore.UserChannels.getOrElse(uid, MSet.empty[UUID]).flatMap(DataStore.Channels.get)
              // channel followed by the user that have same phone number as the channel user is trying to follow
              val channelsFollowedByUserWithSameNo = channelsFollowedByUser.filter(_.phoneNumber == channel.get.phoneNumber)

              // if no channels followed by user have same number as the channel user is trying to follow
              if (channelsFollowedByUserWithSameNo.isEmpty) {
                channel.get.userStartsFollowing(uid)
                Future.successful(Ok(Json.toJson(Json.obj(
                  "success"-> true
                ))))
              } else {
                // channelsFollowedByUserWithSameNo can have maximum size of 1(in case user is trying to follow a channel he is already following)
                channelsFollowedByUserWithSameNo.head match {
                    // user is trying to follow a channel he is already following
                  case ch: Channel if ch.id == cid =>
                    Future.successful(Ok(Json.toJson(Json.obj(
                      "success"-> true,
                      "message"-> "already following this channel"
                    ))))
                  // user is trying to follow a channel he is not following yet
                  case ch: Channel if ch.id != cid =>
                    val passiveChannel = Channel.getLessActiveChannel(ch, channel.get)

                    // get phone number which are not bound to any channel the user is following in the increasing order
                    // of their allocation to channels. If it cannot find any used numbers then it picks an unused number
                    phoneNumberChooser((DataStore.PhoneNumbers -- channelsFollowedByUser.flatMap(_.phoneNumber)).toSeq.sortBy(_._2).toList) match {
                      case head :: tail =>
                        passiveChannel.updatePhoneNo(head._1)
                        channel.get.userStartsFollowing(uid)
                        Future.successful(Ok(Json.toJson(Json.obj(
                          "success" -> true,
                          "channel" -> channel.get
                        ))))
                      case _ =>
                        Future.successful(Ok(Json.toJson(Json.obj(
                          "success" -> false,
                          "message" -> "Cannot allocate phone number"
                        ))))
                    }
                }
              }
            }
          }
        }

      case _ =>
        Future.successful(Ok(Json.toJson(Json.obj(
          "success"-> false,
          "message"-> "Id should be in the form of UUID"
        ))))
    }
  }

  def sendBroadCast(channelId: String) = Action.async { implicit request =>
    getUUID(channelId) match {

      case Success(cid) =>
        val channel = Channel.findById(cid)
        if (channel.isEmpty)
          Future.successful(Ok(Json.toJson(Json.obj(
            "success"-> false,
            "message"-> "No channel with given id"
          ))))
        else {
          synchronized {
            // if the channel has any followers
            if (DataStore.ChannelUsers(cid).nonEmpty) {
              val unow = DateTime.now(DateTimeZone.UTC).getMillis

              // if channel has a phone number
              if (channel.get.phoneNumber.isDefined) {
                val brd = Broadcast(unow, cid, s"Some message by ${channel.get.name}($cid)", DataStore.ChannelUsers(cid).toList)
                brd.insert()
                Future.successful(Ok(Json.toJson(Json.obj(
                  "success"-> true
                ))))
              }
              // channel does not have a number, so try to get one for it
              else {
                // all the phone numbers used by any channel that is followed by any user who is also following this channel
                val phoneNumbersToAvoid = DataStore.ChannelUsers(cid).flatMap { uid =>
                  DataStore.UserChannels.getOrElse(uid, MSet.empty[UUID]).flatMap(DataStore.Channels.get).flatMap(_.phoneNumber)
                }

                // all phone numbers that do not belong to `phoneNumbersToAvoid`. Tries to pick a used number if it can otherwise
                // picks an unused number
                phoneNumberChooser((DataStore.PhoneNumbers -- phoneNumbersToAvoid).toSeq.sortBy(_._2).toList) match {
                  case head :: tail =>
                    channel.get.updatePhoneNo(head._1)
                    val brd = Broadcast(unow, cid, s"Some message by ${channel.get.name}($cid)", DataStore.ChannelUsers(cid).toList)
                    brd.insert()
                    Future.successful(Ok(Json.toJson(Json.obj(
                      "success" -> true
                    ))))
                  case _ =>
                    Future.successful(Ok(Json.toJson(Json.obj(
                      "success" -> false,
                      "message" -> "Cannot allocate phone number"
                    ))))
                }
              }
            }
              // channel has no followers
            else
              Future.successful(Ok(Json.toJson(Json.obj(
                "success"-> true,
                "message"-> "No users to broadcast to"
              ))))
          }
        }
      case _ =>
        Future.successful(Ok(Json.toJson(Json.obj(
          "success"-> false,
          "message"-> "Id should be in the form of UUID"
        ))))
    }
  }

  // for debugging
  def showDataStoreInConsole = Action.async { implicit request =>
    Application.logDataStoreInConsole
    Future.successful(Ok(Json.toJson(Json.obj(
      "success"-> true
    ))))
  }

  // for init data store
  def fillDataStore = Action.async { implicit request =>

    val rawPhoneNumbers = List("91-8800646754", "91-8800646755", "1-3308946776", "1-3308946780", "2-6509846709", "12-8989898989", "32-4545454545")
    val channelNames = List("Channel1", "Channel2", "Channel3")
    val userNames = List("User1", "User2", "User3", "User4", "User5", "User6", "User7")

    rawPhoneNumbers.foreach(PhoneNumber.addNew)
    channelNames.foreach(Channel(getUUID, _, None).insert())
    userNames.foreach(User(getUUID, _).insert())

    Application.logDataStoreInConsole

    Future.successful(Ok(Json.toJson(Json.obj(
      "success"-> true
    ))))
  }
}


object Application {
  def logDataStoreInConsole: Unit = {
    println("******Logging data store start*******")
    println("---Printing phone numbers----")
    println(DataStore.PhoneNumbers)
    println
    println("---Printing channels----")
    println(DataStore.Channels)
    println
    println("---Printing users----")
    println(DataStore.Users)
    println
    println("---Printing channel followers----")
    println(DataStore.ChannelUsers)
    println
    println("---Printing user channels----")
    println(DataStore.UserChannels)
    println
    println("---Printing broadcasts----")
    println(DataStore.Broadcasts)
    println("******Logging data store end*******")
    println
  }
}