package data

import java.util.UUID

import data.models.{Broadcast, User, Channel, PhoneNumber}


object DataStore {
  import scala.collection.mutable.{MutableList => MList}
  import scala.collection.mutable.{Set => MSet}
  import scala.collection.mutable.{Map => MMap}

  // Map of channel UUID as key and Channel object as value
  val Channels = MMap.empty[UUID, Channel]

  // List of users
  val Users = MList.empty[User]

  // Map of PhoneNumber as key and number of channels its assigned to as value
  val PhoneNumbers = MMap.empty[PhoneNumber, Int]

  // For maintaining followers, 2 Maps are used. One is channel to its followers(users) and the other is user to the channels he's following

  // Map of channel UUID as key and set of UUIDs of its followers as value
  val ChannelUsers = MMap.empty[UUID, MSet[UUID]]

  // Map of user UUID as key and set of UUIDs of channels he's following as value
  val UserChannels = MMap.empty[UUID, MSet[UUID]]

  // List of broadcasts
  val Broadcasts = MList.empty[Broadcast]
}
