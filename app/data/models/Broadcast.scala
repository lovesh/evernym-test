package data.models

import java.util.UUID

import data.DataStore

// TODO: Consider saving the channel's current phone number too while saving broadcast. Will help if wanted to know which
// number was used to send most messages

case class Broadcast(timestamp: Long, channelId: UUID, message: String, toUsers: List[UUID]) {

  def insert() {
    DataStore.Broadcasts += this
  }
}