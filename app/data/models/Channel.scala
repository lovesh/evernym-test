package data.models

import java.util.UUID

import data.DataStore
import org.joda.time.{DateTimeZone, DateTime}

case class Channel(id: UUID, name: String, var phoneNumber: Option[PhoneNumber]) {

  def insert() {
    import scala.collection.mutable.{Set => MSet}
    DataStore.Channels += (this.id -> this)
    DataStore.ChannelUsers += (this.id -> MSet.empty[UUID])
  }

  def userStartsFollowing(userId: UUID): Unit = {
    DataStore.ChannelUsers(this.id).add(userId)
    DataStore.UserChannels(userId).add(this.id)
  }

  def updatePhoneNo(pn: PhoneNumber): Unit = {
    if (this.phoneNumber.isDefined)
      DataStore.PhoneNumbers(this.phoneNumber.get) -= 1
    this.phoneNumber = Some(pn)
    DataStore.PhoneNumbers(this.phoneNumber.get) += 1
  }

}

object Channel {

  // Duration for which channel's activity(message sending) has to be checked in case of a number change
  val activityTimeThreshold = 36e5      // milliseconds

  def findById(id: UUID): Option[Channel] = {
    DataStore.Channels.get(id)
  }

  def getLessActiveChannel(ch1: Channel, ch2: Channel): Channel = {
    val unow = DateTime.now(DateTimeZone.UTC).getMillis
    val (ch1Bs, ch2Bs) = DataStore.Broadcasts.filter { b =>
      ((unow - b.timestamp) < activityTimeThreshold) && (b.channelId == ch1.id || b.channelId == ch2.id)
    }.partition(_.channelId == ch1.id)

    val ch1sms = ch1Bs.foldLeft(0)((a,b) => a+b.toUsers.size)
    val ch2sms = ch2Bs.foldLeft(0)((a,b) => a+b.toUsers.size)
    if (ch2sms > ch1sms)
      ch1
    else
      ch2
  }

}
