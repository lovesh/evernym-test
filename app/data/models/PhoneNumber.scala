package data.models

import data.DataStore
import exceptions.PhoneNumberExists
import scala.util.{Success, Failure, Try}

case class PhoneNumber(number: String) {

  require("^\\d{1,2}-\\d{10}$".r findFirstIn number match {   // the phone number should be in the format `<1 or 2 digits>-<10 digits>`
    case Some(m: String) => true
    case None => false
  }, "Invalid phone number")

  // To be used in DataStore.PhoneNumbers and other comparisons
  override def equals(o: Any) = o match {
    case that: PhoneNumber => that.number.equals(this.number)
    case _ => false
  }

  // insert phone number to data store
  def insert: Boolean = {
    synchronized {       // multiple requests might try to add same phone number
      if (DataStore.PhoneNumbers.contains(this))
        false
      else {
        DataStore.PhoneNumbers += (this -> 0)
        true
      }
    }
  }

}

object PhoneNumber {

  def addNew(number: String): Try[PhoneNumber] = {
    try {
      val pn = PhoneNumber(number)
      if (pn.insert)
        Success(pn)
      else
        Failure(PhoneNumberExists)
    } catch {
      case ex: java.lang.IllegalArgumentException =>
        println("phone fail")
        Failure(ex)
    }
  }
}
