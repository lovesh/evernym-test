package data.models

import java.util.UUID

import data.DataStore

case class User(id: UUID, name: String) {

  def insert() {
    import scala.collection.mutable.{Set => MSet}
    DataStore.Users += this
    DataStore.UserChannels += (this.id -> MSet.empty[UUID])
  }

}
