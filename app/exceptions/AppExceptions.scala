package exceptions

import play.api.mvc.Results


trait AppException extends Exception {

  val success = false
  def message: String
  val resultStatus: Results.Status = Results.InternalServerError

  override def fillInStackTrace(): Throwable = {
    // prevent generation of stacktrace to make things faster
    this
  }

  override def getMessage: String = {
    this.message
  }
}

object PhoneNumberExists extends AppException {
  val errorCode = 1
  val message = "Phone number already exists"
}
