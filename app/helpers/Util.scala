package helpers

import java.util.UUID

import data.models.PhoneNumber

import scala.util.{Success, Try, Failure}

object Util {
  def getUUID: UUID = UUID.randomUUID()

  def getUUID(str: String = null): Try[UUID] = {
    try {
      Success(UUID.fromString(str))
    } catch {
      case ex: java.lang.IllegalArgumentException =>
        Failure(ex)
    }
  }

  // takes a list of phone numbers and their usage. If their are any used phone numbers returns a list of used phone numbers
  // else return a list of unused phone numbers
  def phoneNumberChooser(lst: List[(PhoneNumber, Int)]): List[(PhoneNumber, Int)] = {
    lst.partition(_._2 == 0) match {
      case (unusedPhoneNumbers, usedPhoneNumbers) =>
        if (usedPhoneNumbers.isEmpty)
          unusedPhoneNumbers
        else
          usedPhoneNumbers
    }
  }
}
